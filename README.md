# JutgITB.Final


## Descripció
JutgITB és un joc que consta d'una batería de problemes, en aquest cas son 20. Cada exercici pertany a una secció i aquestes seccions poden ser: 
 - tipus de dades
 - condicionals
 - bucles
 - llistes/arrays

Cada problema consta d'un titol, un enunciat, una entrada i una sortida pública que mostrará a l'usuari i una entrada i sortida privades. Quan l'alumne vagi a fer el problema, se li mostrarà l'entrada i la sortida públiques, a continuació se li preguntarà si vol fer el problema, té dues opcions: 
 1. Si vol fer el problema, se li mostrarà l'entrada privada i haurà de respondre, si la seva resposta es igual a la sortida privada, encertarà  el problema i passarà al següent. En cas de que no l'encerti, se li preguntarà si vol tornar-lo a fer, si escull 'si', tindrà un altre intent, en cas contrari, passarà al següent problema. El número d'intents per fe run problema es il·limitat.
 2. En cas de que no vulgui fer el problema, passarà directament al següent problema.

Els problemes crec que tenen un ordre ascendent de dificultat, ja que l'última secció es la més complicada i la menys intuïtiva. 

L'alumne, a més de poder fer els 20 exercicis seguidament, tindrà l'opció d'escollir amb llibertat quin dels 20 exercicis vol fer a través d'un menú on surten els titols, els enunciats dels problemes, així com si l'ha respòs correctament o no. 

L'alumne també podrà veure el seu historial del joc, quins problemes ha respòs bé, quants intents ha fet per problema, quins intents totals a fet a la partida i la seva puntuació actual. 

A més tindrà en el menú principal una opció d'ajuda on podrà entendre el duncionament del JutgITB. 

Finalment, si per qualsevol motiu es cansa de jugar, pot sortir del joc i es guardaran les seves dades a la base de dades creada amb anterioritat. 

## Usuaris extra
A més de poder accedir al JutgITB com alumne per fer els problemes, també s'hi pot accedir com a professor, el cuàl podrà afegir nous problemes a la base de dades i veure l'historial dels alumnes (en aquest cas 1) que hi han registrats i que han jugat anteriorment. 

## Estat del projecte
El proyecte podría expandirse molt més, ara mateix tant l'alumne com el professor s'emmagatzemen en una base de dades local, un arxiu .db insertat al projecte i nomès i pot haber un profesor i un alumne. Quan es crea un nou professor o alumne,  esborra l'antic alumne o professor y s'esborren les dades antigues per poder carregar de noves, l'únic que perdura a la base de dades son els exercicis introduits anteriorment per el professor.

Els id autoincrementats, com els id dels alumnes, del professor, dels intents i de pràcticament totes les taules, no tornen a 1. Per exemple si m'identifico com a alumne i el meu id és 1, al sortir i tornar a entrar com un nou alumne, el meu id d'alumne serà 2 encara que no hi hagi l'anterior alumne a la base de dades perquè s'haurà borrat, això mateix passa amb els professors i amb els id dels intents de l'alumne. 

A l'imatge 'esquema_jutgITB_professor.jpg' es pot veure amb detall quines taules hi han a la base de dades i quins atributs te cada taula, a més es poden veure les claus primaries i com estàn relacionades les taules. 

El projecte també podría emmagatzemar més professors i depenent del professor, poder introduir problemes a la base de dades que nomès certs alumnes hi poguèssin accedir a ells. 

## Bugs trobats al programa
Quán et demana el programa que premis certs botóns, si prems el boto enter, surt un error i el programa es para. 
Quan crees un usuari, el programa es queda en pausa i s'ha de premer el botó enter o un altre perque hi ha un readln() al codi que s'ha d'omplir per anar al menú principal. 
Els enunciats o cadenes llargues de Strings que es troben a la base de dades, surten per consola amb els \n escrits. No he pogut arreglar aquest error.

## Tecnologies utilitzades
Entorn de desenvolupament per a kotlin com pot ser IntellijIdea. 
Entorn de creació de bases de dades en SQLite. 
Pluguin utilitzat a l'IntelliJ per veure en tot moment com s'esborren i s'insereixen dades a la base de dades, com ara SimpleSqliteBrowser.



